package com.cardCost.reactivewebservice.controller;

import static org.mockito.Mockito.when;

import com.cardCost.reactivewebservice.data.model.CountryCost;
import com.cardCost.reactivewebservice.data.model.binlist.BinListBank;
import com.cardCost.reactivewebservice.data.model.binlist.BinListCountry;
import com.cardCost.reactivewebservice.data.model.binlist.BinListNumber;
import com.cardCost.reactivewebservice.data.model.binlist.BinListResponse;
import com.cardCost.reactivewebservice.data.repository.CountryCostRepository;
import com.cardCost.reactivewebservice.webClient.RequestClient;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class CardsCostControllerTest {

    @MockBean
    RequestClient requestClient;
    @Autowired
    private WebTestClient webTestClient;
    @MockBean
    private CountryCostRepository countryCostRepository;

    @Test
    public void testPaymentCardsCost() {

        CountryCost countryCost = CountryCost.builder().country("DK").cost(200.1).build();
        Mono<CountryCost> countryCostMono = Mono.just(countryCost);

        BinListResponse binListResponse = BinListResponse.builder()
            .number(BinListNumber.builder().luhn(true).length(1).build())
            .scheme("test")
            .type("test")
            .brand("tese")
            .prepaid(false)
            .country(
                BinListCountry.builder().numeric("test").alpha2("DK").name("test").emoji("test").currency("test").latitude(1).longitude(1).build())
            .bank(BinListBank.builder().name("test").url("test").phone("test").city("test").build()).build();

        Mono<BinListResponse> binListResponseMono = Mono.just(binListResponse);

        when(countryCostRepository.findByCountry(countryCost.getCountry())).thenReturn(countryCostMono);
        when(requestClient.performTheCall("45717360")).thenReturn(binListResponseMono);

        webTestClient
            .post()
            .uri("/payment-cards-cost")
            .accept(MediaType.APPLICATION_JSON)
            .body(Mono.just(countryCost), CountryCost.class)
            .exchange()
            .expectStatus().isOk()
            .expectBody(Object.class);

    }


}
