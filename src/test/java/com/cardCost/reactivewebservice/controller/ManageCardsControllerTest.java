package com.cardCost.reactivewebservice.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import com.cardCost.reactivewebservice.data.model.CountryCost;
import com.cardCost.reactivewebservice.data.model.ResponseObject;
import com.cardCost.reactivewebservice.data.repository.CountryCostRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ManageCardsControllerTest {

    @Autowired
    private WebTestClient webTestClient;


    @MockBean
    private CountryCostRepository countryCostRepository;


    @Test
    @WithMockUser(username = "admin", roles = {"USER", "ADMIN"})
    public void getAllCountryCosts() {
        CountryCost countryCost1 = CountryCost.builder().id(1L).country("AL").cost(200.1).build();
        CountryCost countryCost2 = CountryCost.builder().id(1L).country("DK").cost(200.1).build();

        List<CountryCost> list = new ArrayList<CountryCost>();
        list.add((countryCost1));
        list.add((countryCost2));

        Flux<CountryCost> countryCostFlux = Flux.fromIterable(list);

        when(countryCostRepository.findAll())
            .thenReturn(countryCostFlux);

        webTestClient
            .get()
            .uri("/admin/all-country-costs")
            .headers(headers -> headers.setBasicAuth("admin", "admin"))
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk()
            .expectBodyList(CountryCost.class)
            .hasSize(2);

        Mockito.verify(countryCostRepository, times(1)).findAll();

    }

    @Test
    @WithMockUser(username = "admin", roles = {"USER", "ADMIN"})
    public void testInsertCountryCost() {

        CountryCost countryCost = CountryCost.builder().country("DK").cost(200.1).build();
        Mono<CountryCost> countryCostMono = Mono.just(countryCost);
        when(countryCostRepository.findByCountry(countryCost.getCountry())).thenReturn(Mono.empty());
        when(countryCostRepository.save(any(CountryCost.class))).thenReturn(countryCostMono);

        webTestClient
            .post()
            .uri("/admin/insert-country-cost")
            .headers(headers -> headers.setBasicAuth("admin", "admin"))
            .accept(MediaType.APPLICATION_JSON)
            .body(Mono.just(countryCost), CountryCost.class)
            .exchange()
            .expectStatus().isCreated();
    }

    @Test
    @WithMockUser(username = "admin", roles = {"USER", "ADMIN"})
    public void testDeleteCountryCostById() {

        CountryCost countryCost = CountryCost.builder().id(1L).country("DK").cost(200.1).build();
        Mono<CountryCost> countryCostMono = Mono.just(countryCost);
        when(countryCostRepository.findById(1L)).thenReturn(countryCostMono);
        when(countryCostRepository.deleteById(1L)).thenReturn(Mono.empty());
        webTestClient
            .delete()
            .uri("/admin/delete-country-cost/1")
            .headers(headers -> headers.setBasicAuth("admin", "admin"))
            .exchange()
            .expectStatus().isOk()
            .expectBody(ResponseObject.class);
    }

    @Test
    @WithMockUser(username = "admin", roles = {"USER", "ADMIN"})
    public void testUpdateCountryCost() {

        CountryCost countryCost = CountryCost.builder().country("DK").cost(200.1).build();
        Mono<CountryCost> countryCostMono = Mono.just(countryCost);
        when(countryCostRepository.findById(1L)).thenReturn(countryCostMono);
        when(countryCostRepository.save(any(CountryCost.class))).thenReturn(countryCostMono);

        webTestClient
            .put()
            .uri("/admin/update-country-cost/1")
            .headers(headers -> headers.setBasicAuth("admin", "admin"))
            .accept(MediaType.APPLICATION_JSON)
            .body(Mono.just(countryCost), CountryCost.class)
            .exchange()
            .expectStatus().isOk()
            .expectBody(ResponseObject.class);
    }


}
