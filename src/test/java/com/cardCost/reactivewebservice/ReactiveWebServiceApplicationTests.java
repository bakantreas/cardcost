package com.cardCost.reactivewebservice;

import com.cardCost.reactivewebservice.controller.CardsCostControllerTest;
import com.cardCost.reactivewebservice.controller.ManageCardsControllerTest;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.reactive.config.EnableWebFlux;

@EnableWebFlux
@SpringBootTest
class ReactiveWebServiceApplicationTests {

	@Test
	void contextLoads() {
	}

}
