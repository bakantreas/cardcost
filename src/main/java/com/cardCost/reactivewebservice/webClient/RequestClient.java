package com.cardCost.reactivewebservice.webClient;

import com.cardCost.reactivewebservice.data.model.binlist.BinListResponse;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
public class RequestClient {

    private final WebClient client;

    /**
     * This client performs calls to the https://lookup.binlist.net
     */
    public RequestClient(WebClient.Builder builder) {
        this.client = builder.baseUrl("https://lookup.binlist.net").build();
    }

    public Mono<BinListResponse> performTheCall(String cart) {
        return this.client.get().uri("/{cart}", cart)
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .bodyToMono(BinListResponse.class);
    }

}
