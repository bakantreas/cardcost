package com.cardCost.reactivewebservice.controller;

import static org.apache.commons.lang3.StringUtils.isNumeric;

import com.cardCost.reactivewebservice.data.model.CardRequest;
import com.cardCost.reactivewebservice.data.model.CardResponse;
import com.cardCost.reactivewebservice.data.model.binlist.BinListCountry;
import com.cardCost.reactivewebservice.data.model.binlist.BinListResponse;
import com.cardCost.reactivewebservice.data.repository.CountryCostRepository;
import com.cardCost.reactivewebservice.webClient.RequestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 * This controller manages the endpoint regarding the clearing const
 */
@RestController
public class CardsCostController {

    @Autowired
    CountryCostRepository countryCostRepository;

    @Autowired
    RequestClient requestClient;

    private static final Logger logger = LoggerFactory.getLogger(CardsCostController.class);


    /**
     * This endpoint receives a card number and returns its  clearing cost
     */
    @PostMapping("/payment-cards-cost")
    public Mono<Object> paymentCardsCost(@RequestBody CardRequest cardRequest) {

        if (null != cardRequest && null != cardRequest.getCardNumber()) {

            String cardNumber = cardRequest.getCardNumber().replaceAll(" ", "");
            if (cardNumber.length() > 7 && cardNumber.length() < 20 && isNumeric(cardNumber)) {
                Mono<BinListResponse> binListResponseMono = requestClient.performTheCall(cardNumber.substring(0, 6));
                Mono<String> alpha2 = binListResponseMono.map(BinListResponse::getCountry).map(BinListCountry::getAlpha2);
                return alpha2.flatMap(s -> {
                    return countryCostRepository.findByCountry(s)
                        .switchIfEmpty(countryCostRepository.findByCountry("Others"))
                        .flatMap(countryCost -> {
                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
                            Date date = new Date(System.currentTimeMillis());
                            logger.info(" Country: {} Cost: {} Date: {}", s, countryCost.getCost(), formatter.format(date));
                            return Mono.just(new CardResponse(s, countryCost.getCost()));
                        });
                });

            } else {
                logger.info(" The card {} is not valid ", cardRequest);
                return Mono.just("The card is not valid");
            }

        } else {
            logger.info(" The card {} is not valid ", cardRequest);
            return Mono.just("The input is not valid.");
        }
    }


}
