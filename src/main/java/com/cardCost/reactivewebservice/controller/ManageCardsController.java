package com.cardCost.reactivewebservice.controller;

import static org.hibernate.query.criteria.internal.ValueHandlerFactory.isNumeric;

import com.cardCost.reactivewebservice.data.model.CountryCost;
import com.cardCost.reactivewebservice.data.model.ResponseObject;
import com.cardCost.reactivewebservice.data.repository.CountryCostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * This controller manages the endpoints regarding the clearing costs administration
 */
@RestController
@RequestMapping("/admin")
public class ManageCardsController {

    private static final String ERROR = "error";
    private static final String SUCCESS = "success";

    @Autowired
    CountryCostRepository countryCostRepository;


    /**
     * This endpoint retrieves the data of the db table COUNTRY_COST
     */
    @GetMapping("/all-country-costs")
    public Flux<CountryCost> allCountryCosts() {
        return countryCostRepository.findAll();
    }

    /**
     * This endpoint inserts info to the COUNTRY_COST db table
     */
    @PostMapping("/insert-country-cost")
    public Mono<ResponseEntity<ResponseObject>> addCountryCost(@RequestBody CountryCost countryCost) {

        if (countryCost.getCost() == null || countryCost.getCost() == null || !isNumeric(countryCost.getCost())) {
            return Mono.just(ResponseEntity
                .badRequest()
                .body(new ResponseObject(ERROR, "The data are not valid.")));
        }

        return countryCostRepository.findByCountry(countryCost.getCountry())
            .flatMap(countryCost1 -> {
                return Mono.just(ResponseEntity
                    .badRequest()
                    .body(new ResponseObject(ERROR, "The CountryCost already exists.")));
            })
            .switchIfEmpty(countryCostRepository.save(countryCost)
                .thenReturn(
                    ResponseEntity.status(HttpStatus.CREATED)
                        .body(new ResponseObject(SUCCESS, "The CountryCost has been added."))));


    }

    /**
     * This endpoint updates the info of the COUNTRY_COST db table
     */
    @PutMapping("/update-country-cost/{id}")
    public Mono<ResponseEntity<ResponseObject>> updateCountryCost(@PathVariable("id") Long id, @RequestBody CountryCost countryCost) {
        return countryCostRepository.findById(id)
            .flatMap(countryCost1 -> {
                countryCost.setId(id);
                return countryCostRepository.save(countryCost)
                    .thenReturn(ResponseEntity
                        .ok()
                        .body(new ResponseObject(SUCCESS, "The CountryCost has been updated.")));
            })
            .switchIfEmpty(Mono.just(ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new ResponseObject(ERROR, "The CountryCost not found."))));
    }

    /**
     * This endpoint deletes info from the COUNTRY_COST db table
     */
    @DeleteMapping("/delete-country-cost/{id}")
    public Mono<ResponseEntity<ResponseObject>> deleteCountryCost(@PathVariable("id") Long id) {
        return countryCostRepository.findById(id).flatMap(countryCost -> {
            return countryCostRepository.deleteById(countryCost.getId())
                .thenReturn(ResponseEntity
                    .ok()
                    .body(new ResponseObject(SUCCESS, "The CountryCost has been deleted.")));
        })
            .switchIfEmpty(Mono.just(ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new ResponseObject(ERROR, "The CountryCost not found."))));

    }

}
