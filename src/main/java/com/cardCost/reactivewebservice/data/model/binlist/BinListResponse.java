package com.cardCost.reactivewebservice.data.model.binlist;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BinListResponse {

    BinListNumber number;
    String scheme;
    String type;
    String brand;
    boolean prepaid;
    BinListCountry country;
    BinListBank bank;

}
