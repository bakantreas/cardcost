package com.cardCost.reactivewebservice.data.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Table("country_cost")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CountryCost {
    @Id
    private Long id;
    String country;
    Double cost;
}
