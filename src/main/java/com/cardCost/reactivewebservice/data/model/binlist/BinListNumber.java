package com.cardCost.reactivewebservice.data.model.binlist;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BinListNumber {
    int length;
    boolean luhn;
}
