package com.cardCost.reactivewebservice.data.model;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CardResponse {
    String country;
    Double cost;
}
