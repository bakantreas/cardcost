package com.cardCost.reactivewebservice.data.model.binlist;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BinListBank {

    String name;
    String url;
    String phone;
    String city;

}
