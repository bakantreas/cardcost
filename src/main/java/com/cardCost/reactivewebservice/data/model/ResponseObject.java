package com.cardCost.reactivewebservice.data.model;


import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseObject {
    String result;
    String message;
}
