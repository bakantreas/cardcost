package com.cardCost.reactivewebservice.data.model.binlist;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BinListCountry {

    String numeric;
    String alpha2;
    String name;
    String emoji;
    String currency;
    int latitude;
    int longitude;

}
