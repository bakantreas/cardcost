package com.cardCost.reactivewebservice.data.repository;

import com.cardCost.reactivewebservice.data.model.CountryCost;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface CountryCostRepository extends ReactiveCrudRepository<CountryCost, Long> {

    Mono<CountryCost> findByCountry(String country);
}
