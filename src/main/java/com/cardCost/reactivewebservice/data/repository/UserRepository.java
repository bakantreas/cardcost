package com.cardCost.reactivewebservice.data.repository;

import com.cardCost.reactivewebservice.data.model.user.User;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface UserRepository extends ReactiveCrudRepository<User, Long> {

    Mono<UserDetails> findByUsername(String username);
}
