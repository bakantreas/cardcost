package com.cardCost.reactivewebservice.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.core.userdetails.MapReactiveUserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.server.SecurityWebFilterChain;

@Configuration
@EnableWebFluxSecurity
class WebFluxSecurity {

    @Bean
    public SecurityWebFilterChain chain(ServerHttpSecurity http) {
        http.csrf().disable()
            // allow all path accessed by all role
            .authorizeExchange()
            .pathMatchers("/payment-cards-cost").permitAll()
            .anyExchange().authenticated().and().httpBasic();

        return http.build();
    }


    @Bean
    public MapReactiveUserDetailsService userDetailsService() {
        UserDetails admin = User.builder()
            .username("admin")
            .password("{noop}admin")
            .roles("ADMIN")
            .build();
        return new MapReactiveUserDetailsService(admin);
    }


}