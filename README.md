**CARD COST API**

This project exposes an API regarding the bank cards clearing costs for each country.

The user will be able to insert his card number and he will receive the clearing cost and the country code that this card has been created

Also, it exposes administration endpoints where the administrator can manage the clearing cost database.

It has been created using java spring boot, and Maven. 
The main dependencies that have been used are the following

* H2 database engine
* Webflux
* lombok
4. junit
* r2dbc-h2
* swagger

---


### Run the application ###

install maven following the below path: https://www.baeldung.com/install-maven-on-windows-linux-mac

You can run the application applying the below actions:

*  mvn package
*  mvn spring-boot:run

Then the Tomcat server will be up and running on the path http://localhost:8080/.

You can check the endpoints that have been generated to the below swagger path:
http://localhost:8080/swagger-ui.html


The following info has been added to the database:

* US $5
* GR $15
* Others $10



The endpoint /payment-cards-cost is public, and you can use it without authentication.

Other endpoints demand basic authentication, and you can access them using the following credentials:
* username: admin
* password: admin


### Owner ###

* Andreas Bakopoulos
* bakantreas@gmail.com
